# CSS
* Devices in pure CSS (https://picturepan2.github.io/devices.css/)

# JavaScript
* JavaScript event Keycodes (http://keycode.info/)

# UI
* Modals
    * Morphing Modal (https://codyhouse.co/gem/morphing-modal-window/)
    * Animate Modal (http://joaopereirawd.github.io/animatedModal.js/)

# IOs
* Convert hexcode to UIColor in both Objective-c and swift http://uicolor.xyz/#/hex-to-ui

# Otros
* Show mobile apps in browsers (https://appetize.io/)